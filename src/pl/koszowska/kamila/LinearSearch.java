package pl.koszowska.kamila;

import java.util.List;

public class LinearSearch<T extends Comparable<T>> extends  Search<T>{
    @Override
    public int doSearch(List<T> list, T serachElement) {
        for (int i=0; i<list.size();i++){
            if (list.get(i).equals(serachElement)){
                return i;
            }
        }
        return -1;
    }
}
