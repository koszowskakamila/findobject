package pl.koszowska.kamila;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        List<Integer> list = new ArrayList<Integer>(Arrays.asList(5,6,2,1,8,3,45,23,66,12));
        Search<Integer> search = new LinearSearch<Integer>();
        Search<Integer> searchB = new BinarySearch<Integer>();

        System.out.println(search.doSearch(list,3));
        System.out.println(searchB.doSearch(list,0));

    }
}
