package pl.koszowska.kamila;

import java.math.BigDecimal;

public class ComplexNumber implements Comparable<ComplexNumber>{

    private double realNumber;
    private double imaginaryUnit;

    public ComplexNumber(double realNumber, double imaginaryUnit) {
        this.realNumber = realNumber;
        this.imaginaryUnit = imaginaryUnit;
    }

    public double getRealNumber() {
        return realNumber;
    }

    public void setRealNumber(double realNumber) {
        this.realNumber = realNumber;
    }

    public double getImaginaryUnit() {
        return imaginaryUnit;
    }

    public void setImaginaryUnit(double imaginaryUnit) {
        this.imaginaryUnit = imaginaryUnit;
    }

    public void add(ComplexNumber complexNumber){
        this.realNumber+=complexNumber.getRealNumber();
        this.imaginaryUnit+=complexNumber.getImaginaryUnit();
    }

    public void substract(ComplexNumber complexNumber){
        this.realNumber-=complexNumber.getRealNumber();
        this.imaginaryUnit-=complexNumber.getImaginaryUnit();
    }

    public void multiply(ComplexNumber complexNumber){
        this.realNumber = this.getRealNumber()*complexNumber.getRealNumber() - this.imaginaryUnit*complexNumber.getImaginaryUnit();
        this.imaginaryUnit = this.realNumber*complexNumber.getImaginaryUnit() + this.imaginaryUnit*complexNumber.getRealNumber();
    }

    public void divide(ComplexNumber complexNumber){
        double tmp = Math.pow(complexNumber.getRealNumber(),2) + Math.pow(complexNumber.getImaginaryUnit(),2);
        this.realNumber = (this.realNumber*complexNumber.getRealNumber() + this.imaginaryUnit*complexNumber.getImaginaryUnit()) / tmp;
        this.imaginaryUnit = (this.imaginaryUnit*complexNumber.getRealNumber() - this.realNumber*complexNumber.getImaginaryUnit()) / tmp;
    }

    @Override
    public String toString() {

        if (imaginaryUnit == 0){ return Double.toString(realNumber);}
        String number = Double.toString(realNumber);
        if (imaginaryUnit > 0){ number += "+"; }
        number += Double.toString(imaginaryUnit) + "i";

        return number;
    }


    @Override
    public int compareTo(ComplexNumber o) {
        if (realNumber == o.getRealNumber() && imaginaryUnit == o.getImaginaryUnit()) {
            return 0;
        }
        return -1;

    }
}
