package pl.koszowska.kamila;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class BinarySearch <T extends Comparable<T>> extends Search<T>{

    @Override
    public int doSearch(List<T> list, T searchElement) {
        list.sort(new Comparator<T>() {
            @Override
            public int compare(T o1, T o2) {
                return o1.compareTo(o2);
            }
        });
        return search(list, searchElement, 0, list.size()-1);
    }

    private int search(List<T> list, T searchElement, int start, int end){
        if (end < start){
            return -1;
        }

        int tmp = start + (end - start)/2;
        if (list.get(tmp).compareTo(searchElement) == 0){
            return tmp;
        }

        if (list.get(tmp).compareTo(searchElement) > 0){
            return search(list, searchElement, start, tmp-1);
        }else {
            return search(list, searchElement, tmp+1, end);
        }

    }

}
