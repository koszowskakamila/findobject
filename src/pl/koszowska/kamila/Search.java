package pl.koszowska.kamila;

import java.util.List;

public abstract class Search <T extends  Comparable<T>>{
    public abstract int doSearch(List<T> list, T seachElement);
}
